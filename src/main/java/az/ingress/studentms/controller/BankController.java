package az.ingress.studentms.controller;

import az.ingress.studentms.domain.Account;
import az.ingress.studentms.domain.Amount;
import az.ingress.studentms.services.BankService;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("account")
@RequiredArgsConstructor
public class BankController {
    private final BankService bankService;

    @SneakyThrows
    @PutMapping("/{id}")
    public void deposit(@PathVariable Long id, @RequestBody Amount requestBody){
        bankService.deposit(id, requestBody.getAmount());
    }

    @GetMapping("/{id}")
    public Account get(@PathVariable Long id){
        return bankService.getAccount(id);
    }
}
