package az.ingress.studentms.controller;

import az.ingress.studentms.domain.Group;
import az.ingress.studentms.domain.Student;
import az.ingress.studentms.services.ManyToManyMapping;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.transaction.Transactional;
import java.util.List;

@RestController
@RequestMapping("/students")
@RequiredArgsConstructor
public class StudentController {

    private final ManyToManyMapping manyToManyMapping;

    @GetMapping("/students")
    public List<Student> getStudent(){
        return manyToManyMapping.getAll();
    }
    @GetMapping("/groups")
//    @Transactional
    public List<Group> getGroups(){
        return manyToManyMapping.findGroups();
    }
}
