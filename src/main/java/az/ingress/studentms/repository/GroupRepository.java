package az.ingress.studentms.repository;

import az.ingress.studentms.domain.Group;
import org.springframework.data.domain.Example;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;

@Repository
public interface GroupRepository extends JpaRepository<Group, Long> {

}
