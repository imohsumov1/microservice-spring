package az.ingress.studentms.services;

import az.ingress.studentms.domain.Account;
import az.ingress.studentms.repository.AccountRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

@Slf4j
@Service
@RequiredArgsConstructor
public class BankService {

    @PersistenceContext
    private EntityManager entityManager;
    private final AccountRepository accountRepository;

    @Transactional
    public void deposit(Long id, Long amount) throws InterruptedException {
        log.trace("Thread id is {} retrieved account is below:",Thread.currentThread().getId());
        Account account = accountRepository.findAccountByLockReadOptimistic(id);
        log.trace("Thread id is {} with account balance is {}",Thread.currentThread().getId(),account.getBalance());
        Thread.sleep(20000);
        account.setBalance(account.getBalance() + amount);
        log.trace("Thread {} updated account {} and version is {}",Thread.currentThread().getId(), account.getBalance(), account.getVersion());

    }

    public Account getAccount(Long id) {
        return accountRepository.findById(id)
                .orElseThrow(RuntimeException::new);
    }


    public void  createAccount() {
        Account account = new Account();
        account.setBalance(100L);
        account.setName("Test");
        accountRepository.save(account);
    }
}
