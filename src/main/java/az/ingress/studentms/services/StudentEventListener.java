package az.ingress.studentms.services;

import az.ingress.studentms.domain.Student;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.PostPersist;
import javax.persistence.PrePersist;

@Slf4j
public class StudentEventListener {
    @PrePersist
    public void logNewUserAttempt(Student student) {
        log.info("Pre Persist:" + student.getName());
    }

    @PostPersist
    public void logCreatedUserAttempt(Student student) {
        log.info("Post Persist:" + student.getId());
    }




}




