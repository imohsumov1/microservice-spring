package az.ingress.studentms.services;

import az.ingress.studentms.domain.Group;
import az.ingress.studentms.domain.Student;
import az.ingress.studentms.repository.GroupRepository;
import az.ingress.studentms.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Set;

@Service
@Slf4j
@RequiredArgsConstructor
public class ManyToManyMapping {
    private final StudentRepository studentRepository;
    private final GroupRepository groupRepository;

    public void createStudent(){
        Student student = new Student();
        student.setName("Babek");
        student.setAge(30);
        student.setHomeAddress("Shamkir");

        Student student1 = new Student();
        student1.setName("Anarinho");
        student1.setAge(25);

        Group ms6 = new Group();
        ms6.setName("ms6");
        ms6.setDescription("Java spring");

        Group linux = new Group();
        linux.setName("Linux intensive");
        linux.setDescription("Linux all commands");

        log.info("Group before {}",ms6);
        groupRepository.save(ms6);
        groupRepository.save(linux);
        log.info("Group after {}",ms6);

        student1.setGroups(Set.of(ms6,linux));
        student.setGroups(Set.of(ms6, linux));

        studentRepository.save(student);
        studentRepository.save(student1);
    }

    public void getStudentById(Long id){
        studentRepository.findById(id)
                .stream().forEach(System.out::println);
    }

    @Transactional
    public List<Student> getAll(){
        return studentRepository.findAll();
    }

    public List<Group> findGroups() {
        return groupRepository.findAll();
    }
}
