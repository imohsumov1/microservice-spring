package az.ingress.studentms.services;

import az.ingress.studentms.annotation.MyOwnAnnotation;
import az.ingress.studentms.domain.Group;
import az.ingress.studentms.domain.Student;
import az.ingress.studentms.repository.GroupRepository;
import az.ingress.studentms.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.io.IOException;
import java.util.Optional;
import java.util.Set;

@Service
@Slf4j
@RequiredArgsConstructor
public class EntityLifeCycles {

//    @PersistenceContext
    private final EntityManagerFactory entityManagerFactory;
    private final StudentRepository studentRepository;
    private final GroupRepository groupRepository;

//    @Transactional
    public void testMethodForEverything() {
        log.debug("BEGINNING _________________________________");
        Group groupOrient = new Group();
        groupOrient.setName("JAVA SE");
        groupOrient.setDescription("Basics of Java");

        Group groupBritish = new Group();
        groupBritish.setName("IELTS");
        groupBritish.setDescription("Advanced english");

        Student studentCeyhun = new Student();
        studentCeyhun.setName("Isi");
        studentCeyhun.setSchool("BMS");
        studentCeyhun.setGrade(5L);
        studentCeyhun.setHomeAddress("Nicosia");

        studentCeyhun.setGroups(Set.of(groupOrient, groupBritish));
        log.debug("saving student {}",studentCeyhun);
        studentRepository.save(studentCeyhun);
        log.debug("saved student {}",studentCeyhun);

        log.debug("Delete by id {}",studentRepository.getById(21L));
        studentRepository.deleteById(21L);

        /*  --student saving and deleting
        studentRepository.save(studentCeyhun);
        Optional<Student> byId = studentRepository.findById(19L);
        byId.ifPresent((student -> studentRepository.deleteById(student.getId())));
        */

       /* --group saving and deleting by id--
        groupRepository.save(groupOrient);

        log.debug("Trying to delete {}", groupRepository.getById(11L));
        groupRepository.deleteById(11L);
        */

    }
//    @Transactional(dontRollbackOn = IOException.class)
    @MyOwnAnnotation
    @SneakyThrows
    public void testForLifeCycle() {
        Student s1 = new Student();
        s1.setAge(21);
        s1.setSchool("BMS");
        s1.setName("Isus");
        studentRepository.save(s1);
        s1.setName("Zaur");
        throw new IOException();
    }
}
