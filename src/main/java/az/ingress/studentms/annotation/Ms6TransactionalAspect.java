package az.ingress.studentms.annotation;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import java.util.Scanner;

@Slf4j
@Aspect
@Component
public class Ms6TransactionalAspect {

    @Around("@annotation(MyOwnAnnotation)")
    public Object logExecutionTime(ProceedingJoinPoint joinPoint) throws Throwable {
        log.debug("Started");
        Object proceed = null;
        try{
            proceed= joinPoint.proceed();
        }catch (Exception e){
            System.err.println("Exception caught");
        }
        log.debug("Ended");
        return proceed;
    }
}
