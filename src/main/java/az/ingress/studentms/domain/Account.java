package az.ingress.studentms.domain;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private Long balance;

    @Version
    private Long version;
}
