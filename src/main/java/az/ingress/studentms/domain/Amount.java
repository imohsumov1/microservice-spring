package az.ingress.studentms.domain;

import lombok.Data;

@Data
public class Amount {
    private Long amount;
}
