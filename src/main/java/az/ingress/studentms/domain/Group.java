package az.ingress.studentms.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Data
@Table(name = "i_groups")
public class Group {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String description;

//    @JsonIgnore
    @ManyToMany(mappedBy = "groups",fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    private Set<Student> students = new HashSet<>();
}
