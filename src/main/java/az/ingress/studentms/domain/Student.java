package az.ingress.studentms.domain;

import az.ingress.studentms.services.StudentEventListener;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;

import javax.persistence.*;
@Entity
@Data
@Slf4j
@EqualsAndHashCode(exclude = "groups")
@ToString(exclude = "groups")
@Table(name = "students")
//@EntityListeners(StudentEventListener.class)
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private Integer age;

    private String homeAddress;

    private String school;
    private String description;
    private Long grade;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    @JoinTable(
            name = "groups_students",
            joinColumns = {@JoinColumn(name = "s_id", referencedColumnName = "id")},
                inverseJoinColumns = {@JoinColumn(name = "g_id", referencedColumnName = "id")})
    private java.util.Set<Group> groups = new java.util.HashSet<>();



    }
