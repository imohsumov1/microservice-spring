package az.ingress.studentms;

import az.ingress.studentms.domain.Student;
import az.ingress.studentms.repository.StudentRepository;
import az.ingress.studentms.services.BankService;
import az.ingress.studentms.services.EntityLifeCycles;
import az.ingress.studentms.services.ManyToManyMapping;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.domain.Example;

@SpringBootApplication
@RequiredArgsConstructor
public class StudentMsApplication implements CommandLineRunner {

	private final StudentRepository studentRepository;
	private final ManyToManyMapping manyToManyMapping;
	private final EntityLifeCycles entity;
	private final BankService bankService;


	public static void main(String[] args) {
		SpringApplication.run(StudentMsApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		bankService.createAccount();
	}
}
