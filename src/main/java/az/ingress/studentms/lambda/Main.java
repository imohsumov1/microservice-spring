package az.ingress.studentms.lambda;

public class Main {
    public static void main(String[] args) {
        DemoInterface demoInterface = new DemoInterface() {
            @Override
            public void run() {
                System.out.println("Anonymous class");
            }
        };
        DemoInterface runWithLambda =() -> System.out.println("Anonymous with lambda expression");

        demoInterface.run();
        runWithLambda.run();
    }
}

